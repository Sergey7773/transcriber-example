package app.transcriber.example.operators;

import app.transcriber.api.annotations.MappingComponentName;
import app.transcriber.api.annotations.MappingOperator;
import lombok.AllArgsConstructor;

import java.util.function.UnaryOperator;

@MappingOperator
@MappingComponentName("Substring")
@AllArgsConstructor
public class SubstringMappingOperator implements UnaryOperator<String> {

    private Integer startIndex;
    private Integer endIndex;

    @Override
    public String apply(String s) {
        return s.substring(startIndex, endIndex);
    }

}
