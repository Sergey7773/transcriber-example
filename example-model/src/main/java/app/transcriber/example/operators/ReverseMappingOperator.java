package app.transcriber.example.operators;

import app.transcriber.api.annotations.MappingComponentName;
import app.transcriber.api.annotations.MappingOperator;

import java.util.function.UnaryOperator;

@MappingOperator
@MappingComponentName("Reverse")
public class ReverseMappingOperator implements UnaryOperator<String> {
    @Override
    public String apply(String s) {
        return new StringBuilder(s).reverse().toString();
    }
}
