package app.transcriber.example.source;

import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.List;

@Getter
@Setter
public class Source {

    private String name;
    private String mail;
    private SourceSubEntity sourceSubEntity;
    private List<String> primitiveSourceCol;
    private Collection<SourceSubEntity> subEntitiesSourceCol;

}
