package app.transcriber.example.source;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SourceSubEntity {

    private String sourceSubEntityField;

}
