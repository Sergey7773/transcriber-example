package app.transcriber.example.producers;

import app.transcriber.api.annotations.MappingComponentName;
import app.transcriber.api.annotations.ValuesProducer;

import java.util.UUID;
import java.util.function.Supplier;

@ValuesProducer
@MappingComponentName("IdProducer")
public class IdProducer implements Supplier<String> {
    @Override
    public String get() {
        return UUID.randomUUID().toString();
    }
}
