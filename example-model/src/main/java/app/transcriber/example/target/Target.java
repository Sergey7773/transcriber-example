package app.transcriber.example.target;

import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
public class Target {

    private String firstName;
    private String eMail;
    private TargetSubEntity targetSubEntity;
    private Collection<String> primitiveTargetCol;
    private Collection<TargetSubEntity> subEntitiesTargetCol;
    private String producedField;
    private String constantField;
    private String subEntityFetchedField;

}
