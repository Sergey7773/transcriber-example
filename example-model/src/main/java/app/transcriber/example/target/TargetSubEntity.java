package app.transcriber.example.target;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TargetSubEntity {

    private String targetSubEntityField;

}
