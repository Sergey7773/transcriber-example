package app.transcriber.example.controllers;

import app.transcriber.TranscriberConfigurationProperties;
import app.transcriber.api.exceptions.GenerationInputVerificationException;
import app.transcriber.api.model.TypesMapperDefinition;
import app.transcriber.mapper.reloading.MapperClassReloadingService;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/reload")
@ConditionalOnProperty(prefix = TranscriberConfigurationProperties.PROPERTIES_PREFIX,
        name = TranscriberConfigurationProperties.ENABLE_MAPPERS_RELOADING, havingValue = "true")
public class MappersReloadingController {

    @Autowired
    private MapperClassReloadingService mapperClassReloadingService;

    @PostMapping("/mappers")
    public void reloadMappers(@RequestBody String requestBody) throws GenerationInputVerificationException {
        Collection<TypesMapperDefinition> typesMapperDefinitions =
                new GsonBuilder()
                        .create().fromJson(requestBody,
                        new TypeToken<List<TypesMapperDefinition>>() {
                        }.getType());

        mapperClassReloadingService.reloadMappers(typesMapperDefinitions);
    }
}
