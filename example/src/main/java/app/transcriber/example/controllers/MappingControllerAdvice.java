package app.transcriber.example.controllers;

import app.transcriber.api.exceptions.MapperCreationException;
import app.transcriber.api.exceptions.MappingException;
import com.google.gson.JsonObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class MappingControllerAdvice {


    @ExceptionHandler(value = {MapperCreationException.class, MappingException.class})
    public ResponseEntity<JsonObject> handleException(Exception e, WebRequest request) {
        JsonObject responseBody = new JsonObject();
        responseBody.addProperty("exceptionType", e.getClass().getName());
        responseBody.addProperty("exceptionMessage", e.getMessage());

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseBody);
    }

}
