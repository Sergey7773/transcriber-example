package app.transcriber.example.controllers;

import app.transcriber.api.exceptions.MapperCreationException;
import app.transcriber.api.exceptions.MappingException;
import app.transcriber.api.interfaces.Mapper;
import app.transcriber.api.interfaces.MapperFactory;
import app.transcriber.example.source.Source;
import app.transcriber.example.target.Target;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/mapping")
public class MappingController {

    @Autowired
    private MapperFactory mapperFactory;

    @PostMapping("/mapEntities")
    public ResponseEntity<List<Target>> mapEntities(@RequestBody List<Source> sourceEntities) throws MapperCreationException, MappingException {
        Mapper<Source, Target> sourceToTargetMapper = mapperFactory.createMapper(Source.class, Target.class);

        List<Source> sourcesEntitiesToMap = Optional.ofNullable(sourceEntities).orElse(Collections.emptyList());

        List<Target> targetEntities = new LinkedList<>();

        for (Source sourceEntity : sourcesEntitiesToMap) {
            targetEntities.add(sourceToTargetMapper.apply(sourceEntity));
        }

        return ResponseEntity.ok().body(targetEntities);
    }

}
